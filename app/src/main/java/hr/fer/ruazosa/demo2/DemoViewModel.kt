package hr.fer.ruazosa.demo2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DemoViewModel: ViewModel() {
    var demoModel = DemoModelLiveData()

    override fun onCleared() {
        super.onCleared()
        // called when Activity is destroyed - housekeeping logic
    }
}

class DemoModelLiveData: MutableLiveData<DemoModel>() {
    override fun onActive() {
        super.onActive()
        // TBD called when at least one observer is registered
    }

    override fun onInactive() {
        super.onInactive()
        // TBD called when there is no active observers
    }
}