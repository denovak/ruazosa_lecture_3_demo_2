package hr.fer.ruazosa.demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                DemoViewModel::class.java)

        viewModel.demoModel.value = DemoModel(numberOfClicks = 0)

        viewModel.demoModel.observe(this, Observer { demoModel ->

            numberOfClicks.text = "Number of clicks: " + demoModel.numberOfClicks

        })

        countClickButton.setOnClickListener {
            val demoModel = viewModel.demoModel.value
            if (demoModel != null) {
                demoModel.numberOfClicks = demoModel.numberOfClicks + 1
                viewModel.demoModel.value = demoModel
            }
        }


    }
}
